# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete("qwertyuiopasdfghjklzxcvbnm")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    str[(str.length/2)-1,2]
  else
    str[(str.length/2)]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  counter = 0
  VOWELS.each {|ch| counter += str.count(ch)}
  return counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  if num == 1
    1
  else
    factorial(num-1)*num
  end
end


# MEDIUM
# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  if !(arr.empty?)
    string << arr[0]
    arr.shift
    arr.each {|x| string << separator + x}
  end
  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.each_char.with_index do |ch,i|
    if i.even?
      str[i] = ch.downcase
    else
      str[i] = ch.upcase
    end
  end
  return str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split(" ")
  arr.each_with_index do |word,i|
    if word.length > 4
      arr[i] = word.reverse
    end
  end
  arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []
  i = 1
  while i <= n
    if i % 3 == 0 && i % 5 != 0
      arr.push("fizz")
      i += 1
    elsif i % 3 != 0 && i % 5 == 0
      arr.push("buzz")
      i += 1
    elsif i % 3 == 0 && i % 5 == 0
      arr.push("fizzbuzz")
      i += 1
    else
      arr.push(i)
      i += 1
    end
  end
  return arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  while !(arr.empty?)
    new_arr.push(arr.last)
    arr.pop
  end
  return new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  else
    i = 2
    while i < num
      if num % i == 0
        return false
      else
        i += 1
      end
    end
    return true
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = [1]
  i = 2
  while i <= num
    if num % i == 0
      arr.push(i)
      i += 1
    else
      i += 1
    end
  end
  return arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  arr = factors(num)
  new_arr = []
  arr.each do |n|
    if prime?(n)
      new_arr.push(n)
    end
  end
  return new_arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  return prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each do |n|
    if n.odd?
      odd.push(n)
    else
      even.push(n)
    end
  end
  if odd.length > 1
    return even[0]
  else
    return odd[0]
  end
end
